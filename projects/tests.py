from unittest import skip
from django.contrib.auth.models import User
from django.test import TestCase, Client, RequestFactory
from django.urls import reverse
from .models import Task, Project, ProjectCategory, TaskOffer
from .views import get_user_task_permissions, project_view


class UserTaskPermissionsTests(TestCase):
    owner_dict = {
        'write': True,
        'read': True,
        'modify': True,
        'owner': True,
        'upload': True,
    }

    accepted_offer_dict = {
        'write': True,
        'read': True,
        'modify': True,
        'owner': False,
        'upload': True,
    }

    no_permission_dict = {
        'write': False,
        'read': False,
        'modify': False,
        'owner': False,
        'view_task': False,
        'upload': False,
    }

    def setUp(self):
        user_owner = User.objects.create(username='owner')
        profile_owner = user_owner.profile
        category = ProjectCategory.objects.create(name='test_file.txt')
        project = Project.objects.create(title='owner_test', category=category, user=profile_owner)
        task = Task.objects.create(title='owner_test', project=project)
        user_acc = User.objects.create(username='accepted offer')
        profile_acc = user_acc.profile
        TaskOffer.objects.create(task=task, offerer=profile_acc, status='a')

    def test_task_permissions_owner(self):
        user = User.objects.get(username='owner')
        task = Task.objects.get(title='owner_test')
        self.assertEqual(get_user_task_permissions(user, task), self.owner_dict)

    def test_task_permissions_offerer(self):
        user = User.objects.get(username='accepted offer')
        task = Task.objects.get(title='owner_test')
        self.assertEqual(get_user_task_permissions(user, task), self.accepted_offer_dict)

    def test_task_no_permissions(self):
        user = User.objects.create(username='user')
        task = Task.objects.get(title='owner_test')
        self.assertEqual(get_user_task_permissions(user, task), self.no_permission_dict)


class ProjectViewTests(TestCase):

    def setUp(self):
        user_owner = User.objects.create(username='owner')
        User.objects.create(username='offerer')
        profile_owner = user_owner.profile
        category = ProjectCategory.objects.create(name='test_file.txt')
        Project.objects.create(title='owner_test', category=category, user=profile_owner)

    def test_project_tasks(self):
        user = User.objects.get(username='owner')
        user.set_password('JR@eH@W6LA])7JB#?fzBJb96=?nxtMY.gEWn')
        user.save()
        c = Client()
        c.login(username='owner', password='JR@eH@W6LA])7JB#?fzBJb96=?nxtMY.gEWn')
        project = Project.objects.get(title='owner_test')
        Task.objects.create(title='task1', project=project)
        Task.objects.create(title='task2', project=project)
        Task.objects.create(title='task3', project=project)
        response = c.post(reverse('project_view', kwargs={'project_id': project.id}))
        self.assertEqual(len(response.context['tasks']), 3)

    def test_offer_response(self):
        user = User.objects.get(username='owner')
        user.set_password('JR@eH@W6LA])7JB#?fzBJb96=?nxtMY.gEWn')
        user.save()
        c = Client()
        c.login(username='owner', password='JR@eH@W6LA])7JB#?fzBJb96=?nxtMY.gEWn')
        profile_offerer = User.objects.get(username='offerer').profile
        project = Project.objects.get(title='owner_test')
        task = Task.objects.create(title='task1', project=project)
        task_offer = TaskOffer.objects.create(
            task=task,
            title='hmm',
            description='hmm',
            price=30,
            offerer=profile_offerer,
            status='a',
            feedback='hmm')
        response = c.post(reverse('project_view', kwargs={'project_id': project.id}),
                          {'offer_response': True,
                           'taskofferid': task_offer.id,
                           'title': 'hmm',
                           'description': 'hmm',
                           'price': 30,
                           'offerer': profile_offerer,
                           'status': 'a',
                           'feedback': 'hmm',
                           })
        self.assertEqual(response.context['project'].user, user.profile)

    def test_status_change(self):
        user = User.objects.get(username='owner')
        user.set_password('JR@eH@W6LA])7JB#?fzBJb96=?nxtMY.gEWn')
        user.save()
        c = Client()
        c.login(username='owner', password='JR@eH@W6LA])7JB#?fzBJb96=?nxtMY.gEWn')
        project = Project.objects.get(title='owner_test')
        response = c.post(reverse('project_view', kwargs={'project_id': project.id}),
                          {
                              'status_change': True,
                              'status': 'f',
                          })
        self.assertEqual(response.context['project'].status, 'f')

    def test_not_owner(self):
        user = User.objects.get(username='offerer')
        user.set_password('JR@eH@W6LA])7JB#?fzBJb96=?nxtMY.gEWn')
        user.save()
        c = Client()
        c.login(username='offerer', password='JR@eH@W6LA])7JB#?fzBJb96=?nxtMY.gEWn')
        profile_offerer = user.profile
        project = Project.objects.get(title='owner_test')
        task = Task.objects.create(title='task1', project=project)
        task_offer = TaskOffer.objects.create(
            task=task,
            title='hmm',
            description='hmm',
            price=30,
            offerer=profile_offerer,
            status='a',
            feedback='hmm')

        response = c.post(reverse('project_view', kwargs={'project_id': project.id}),
                          {
                              'offer_submit': True,
                              'taskvalue': task_offer.id,
                              'title': 'hmm',
                              'description': 'hmm',
                              'price': 30,
                              'offerer': profile_offerer,
                              'status': 'a',
                              'feedback': 'hmm',
                          })
        self.assertEqual(response.context['project'].status, 'o')


class TaskOffersTests(TestCase):

    def setUp(self):
        user_owner = User.objects.create(username='owner')
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username='jacob', email='jacob@…', password='top_secret')

        profile = user_owner.profile
        category = ProjectCategory.objects.create(name='test_file.txt')
        project = Project.objects.create(title='owner_test', category=category, user=profile)
        Task.objects.create(title='task1', project=project)

    @skip('Negative prices should not be allowed')
    def test_give_offer(self):
        project = Project.objects.get(title='owner_test')
        task = Task.objects.get(title='task1')
        request = self.factory.post(reverse('project_view', kwargs={'project_id': project.id}),
                                    {
                                        'offer_submit': True,
                                        'taskvalue': task.id,
                                        'title': 'no',
                                        'description': 'should not work',
                                        'price': -20,
                                    })
        request.user = self.user
        project_view(request, project.id)
        with self.assertRaisesMessage(TaskOffer.DoesNotExist, "TaskOffer matching query does not exist."):
            TaskOffer.objects.get(title='no')

    def test_accept_offer(self):
        project = Project.objects.get(title='owner_test')
        task = Task.objects.create(title='task1', project=project)
        task_offer = TaskOffer.objects.create(
            task=task,
            title='hmm',
            description='hmm',
            price=30,
            offerer=self.user.profile,
            status='p',
            feedback='')
        request = self.factory.post(reverse('project_view', kwargs={'project_id': project.id}),
                                    {
                                        'offer_response': True,
                                        'taskofferid': task_offer.id,
                                        'status': 'a',
                                        'feedback': 'hmm',
                                    })
        request.user = User.objects.get(username='owner')
        self.assertEqual(TaskOffer.objects.get(title='hmm').status, 'p')
        project_view(request, project.id)
        self.assertEqual(TaskOffer.objects.get(title='hmm').status, 'a')


class ProjectTemplateTest(TestCase):

    def setUp(self):
        user_owner = User.objects.create(username='owner')
        profile_owner = user_owner.profile
        category = ProjectCategory.objects.create(name='test_file.txt')
        Project.objects.create(title='owner_test', category=category, user=profile_owner,
                               description='This template works')

    def test_template(self):
        user = User.objects.get(username='owner')
        project = Project.objects.get(title='owner_test')
        user.set_password('JR@eH@W6LA])7JB#?fzBJb96=?nxtMY.gEWn')
        user.save()
        c = Client()
        c.login(username='owner', password='JR@eH@W6LA])7JB#?fzBJb96=?nxtMY.gEWn')
        response = c.post(reverse('new_project'), {'template': project.id})
        template = response.context['form']
        self.assertEqual(template.cleaned_data['description'], 'This template works')


class TaskActionTest(TestCase):

    def setUp(self):
        user_owner = User.objects.create(username='owner')
        profile = user_owner.profile
        category = ProjectCategory.objects.create(name='test_file.txt')
        project = Project.objects.create(title='owner_test', category=category, user=profile)
        Task.objects.create(title='task1', project=project)

    def test_delivery(self):
        user = User.objects.get(username='owner')
        user.set_password('JR@eH@W6LA])7JB#?fzBJb96=?nxtMY.gEWn')
        user.save()
        c = Client()
        c.login(username='owner', password='JR@eH@W6LA])7JB#?fzBJb96=?nxtMY.gEWn')
        project = Project.objects.get(title='owner_test')
        task = Task.objects.get(title='task1')
        response = c.post(reverse('task_view', kwargs={'project_id': project.id, 'task_id': task.id}),
                         {
                             'delivery': True,
                             'team': True,
                             'team-add': True,
                             'permissions': True,
                         })
        self.assertEqual(response.status_code, 200)

    def test_task_file(self):
        user = User.objects.get(username='owner')
        user.set_password('JR@eH@W6LA])7JB#?fzBJb96=?nxtMY.gEWn')
        user.save()
        c = Client()
        c.login(username='owner', password='JR@eH@W6LA])7JB#?fzBJb96=?nxtMY.gEWn')
        project = Project.objects.get(title='owner_test')
        task = Task.objects.get(title='task1')
        with open('projects/file_for_testing.txt') as fp:
            response = c.post(reverse('upload_file_to_task', kwargs={'project_id': project.id, 'task_id': task.id}),
                              {
                                  'file': fp,
                              })
        self.assertEqual(response.status_code, 302)
