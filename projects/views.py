from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.core import mail
from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site
from django.utils import timezone

from user.models import Profile
from .forms import ProjectForm, TaskFileForm, ProjectStatusForm, TaskOfferForm, TaskOfferResponseForm, \
    TaskPermissionForm, DeliveryForm, TaskDeliveryResponseForm, TeamForm, TeamAddForm
from .models import Project, Task, TaskFile, TaskOffer, Delivery, ProjectCategory, Team, TaskFileTeam, directory_path


def projects(request):
    all_projects = Project.objects.all()
    project_categories = ProjectCategory.objects.all()
    return render(request,
                  'projects/projects.html',
                  {
                      'projects': all_projects,
                      'project_categories': project_categories,
                  }
                  )


@login_required
def new_project(request):
    current_site = get_current_site(request)
    all_projects = Project.objects.all()
    if request.method == 'POST' and 'template' in request.POST:
        template = Project.objects.get(pk=request.POST['template'])
        form = ProjectForm({'title': template.title, 'description': template.description,
                            'category_id': template.category.pk})
        return render(request, 'projects/new_project.html', {'form': form, 'projects': all_projects})
    elif request.method == 'POST' and ProjectForm(request.POST).is_valid():
        return post_project(request, ProjectForm(request.POST), current_site)
    else:
        form = ProjectForm()
    return render(request, 'projects/new_project.html', {'form': form, 'projects': all_projects})


def post_project(request, form, current_site):
    project = form.save(commit=False)
    project.user = request.user.profile
    project.category = get_object_or_404(ProjectCategory, id=request.POST.get('category_id'))
    project.save()

    people = Profile.objects.filter(categories__id=project.category.id)
    for person in people:
        if person.user.email:
            send_new_project_alert(request, person, project, current_site)
    task_title = request.POST.getlist('task_title')
    task_description = request.POST.getlist('task_description')
    task_budget = request.POST.getlist('task_budget')
    for i in range(0, len(task_title)):
        Task.objects.create(
            title=task_title[i],
            description=task_description[i],
            budget=task_budget[i],
            project=project,
        )
    return redirect('project_view', project_id=project.id)


def send_new_project_alert(request, person, project, current_site):
    try:
        with mail.get_connection() as connection:
            mail.EmailMessage(
                "New Project: " + project.title,
                "A new project you might be interested in was created and can be viwed at " +
                current_site.domain + '/projects/' + str(
                    project.id), "Agreelancer", [person.user.email],
                connection=connection,
            ).send()
    except Exception as e:
        messages.success(request, 'Sending of email to ' + person.user.email + " failed: " + str(e))


def project_view(request, project_id):
    project = Project.objects.get(pk=project_id)
    tasks = project.tasks.all()
    total_budget = 0

    for item in tasks:
        total_budget += item.budget

    if request.user == project.user.user:
        return render_project_owner(request, project, tasks, total_budget)
    else:
        return render_project(request, project, tasks, total_budget)


def render_project_owner(request, project, tasks, total_budget):
    if request.method == 'POST' and 'offer_response' in request.POST:
        instance = get_object_or_404(TaskOffer, id=request.POST.get('taskofferid'))
        offer_response_form = TaskOfferResponseForm(request.POST, instance=instance)
        if offer_response_form.is_valid():
            handle_offer_response(offer_response_form)
    offer_response_form = TaskOfferResponseForm()

    status_form = ProjectStatusForm(request.POST)
    if request.method == 'POST' and 'status_change' in request.POST and status_form.is_valid():
        project_status = status_form.save(commit=False)
        project.status = project_status.status
        project.save()
    status_form = ProjectStatusForm(initial={'status': project.status})

    return render(request, 'projects/project_view.html', {
        'project': project,
        'tasks': tasks,
        'status_form': status_form,
        'total_budget': total_budget,
        'offer_response_form': offer_response_form,
    })


def handle_offer_response(offer_response_form):
    offer_response = offer_response_form.save(commit=False)
    if offer_response.status == 'a':
        offer_response.task.read.add(offer_response.offerer)
        offer_response.task.write.add(offer_response.offerer)
        project = offer_response.task.project
        project.participants.add(offer_response.offerer)
    offer_response.save()


def render_project(request, project, tasks, total_budget):
    if request.method == 'POST' and 'offer_submit' in request.POST:
        task_offer_form = TaskOfferForm(request.POST)
        if task_offer_form.is_valid():
            task_offer = task_offer_form.save(commit=False)
            task_offer.task = Task.objects.get(pk=request.POST.get('taskvalue'))
            task_offer.offerer = request.user.profile
            task_offer.save()
    task_offer_form = TaskOfferForm()

    return render(request, 'projects/project_view.html', {
        'project': project,
        'tasks': tasks,
        'task_offer_form': task_offer_form,
        'total_budget': total_budget,
    })


def is_authorized_to_upload(user_permissions):
    return user_permissions['owner'] or \
           user_permissions['upload'] or \
           user_permissions['write'] or \
           user_permissions['modify']


@login_required
def upload_file_to_task(request, project_id, task_id):
    project = Project.objects.get(pk=project_id)
    task = Task.objects.get(pk=task_id)
    user_permissions = get_user_task_permissions(request.user, task)

    if is_authorized_to_upload(user_permissions):
        if request.method == 'POST' and TaskFileForm(request.POST, request.FILES).is_valid():
            return post_helper(request, project, task, user_permissions)

        task_file_form = TaskFileForm()
        return render(
            request,
            'projects/upload_file_to_task.html',
            {
                'project': project,
                'task': task,
                'task_file_form': task_file_form,
            }
        )
    return redirect('/user/login')


def post_helper(request, project, task, user_permissions):
    task_file = TaskFileForm(request.POST, request.FILES).save(commit=False)
    task_file.task = task
    existing_file = task.files.filter(file=directory_path(task_file, task_file.file.file)).first()
    access = user_permissions['modify'] or user_permissions['owner']
    for team in request.user.profile.teams.all():
        file_modify_access = TaskFileTeam.objects.filter(team=team, file=existing_file, modify=True).exists()
        access = access or file_modify_access
    if access:
        handle_task_file_team(request, project, existing_file, task_file)
    else:
        messages.warning(request, "You do not have access to modify this file")

    return redirect('task_view', project_id=project.id, task_id=task.id)


def handle_task_file_team(request, project, existing_file, task_file):
    if existing_file:
        existing_file.delete()
    task_file.save()
    update_task_file_team(request, project, task_file.task, task_file)


def update_task_file_team(request, project, task, task_file):
    if request.user.profile != project.user and request.user.profile != task.accepted_task_offer.offerer:
        teams = request.user.profile.teams.filter(task__id=task.id)
        for team in teams:
            tft = TaskFileTeam()
            tft.team = team
            tft.file = task_file
            tft.read = True
            tft.save()


def get_user_task_permissions(user, task):
    if user == task.project.user.user:
        return {
            'write': True,
            'read': True,
            'modify': True,
            'owner': True,
            'upload': True,
        }
    if task.accepted_task_offer() and task.accepted_task_offer().offerer == user.profile:
        return {
            'write': True,
            'read': True,
            'modify': True,
            'owner': False,
            'upload': True,
        }
    user_permissions = {
        'write': False,
        'read': False,
        'modify': False,
        'owner': False,
        'view_task': False,
        'upload': False,
    }
    user_permissions['read'] = user_permissions['read'] or user.profile.task_participants_read.filter(
        id=task.id).exists()

    # Team members can view its teams tasks
    user_permissions['upload'] = user_permissions['upload'] or user.profile.teams.filter(task__id=task.id,
                                                                                         write=True).exists()
    user_permissions['view_task'] = user_permissions['view_task'] or user.profile.teams.filter(
        task__id=task.id).exists()

    user_permissions['write'] = user_permissions['write'] or user.profile.task_participants_write.filter(
        id=task.id).exists()
    user_permissions['modify'] = user_permissions['modify'] or user.profile.task_participants_modify.filter(
        id=task.id).exists()

    return user_permissions


@login_required
def task_view(request, project_id, task_id):
    project = Project.objects.get(pk=project_id)
    task = Task.objects.get(pk=task_id)

    user_permissions = get_user_task_permissions(request.user, task)
    if not is_authorized_to_view(user_permissions):
        return redirect('/user/login')

    if request.method == 'POST':
        handle_post(request, task)

    deliver_form = DeliveryForm()
    deliver_response_form = TaskDeliveryResponseForm()
    team_form = TeamForm()
    team_add_form = TeamAddForm()
    deliveries = task.delivery.all()

    team_files, per = team_files_helper(task)
    return render(request, 'projects/task_view.html', {
        'task': task,
        'project': project,
        'user_permissions': user_permissions,
        'deliver_form': deliver_form,
        'deliveries': deliveries,
        'deliver_response_form': deliver_response_form,
        'team_form': team_form,
        'team_add_form': team_add_form,
        'team_files': team_files,
        'per': per
    })


def team_files_helper(task):
    team_files = []
    per = {}
    for file in task.files.all():
        per[file.name()] = {}
        for p in file.teams.all():
            per[file.name()][p.team.name] = p
            if p.read:
                team_files.append(p)
    return team_files, per


def handle_post(request, task):
    user = request.user
    accepted_task_offer = task.accepted_task_offer()

    if 'delivery' in request.POST:
        handle_delivery(request, user, task, accepted_task_offer)

    if 'delivery-response' in request.POST:
        handle_delivery_response(request, user, task)

    if 'team' in request.POST:
        handle_team(request, user, task, accepted_task_offer)

    if 'team-add' in request.POST:
        handle_team_add(request, user, accepted_task_offer)

    if 'permissions' in request.POST:
        handle_permissions(request, user, task, accepted_task_offer)


def handle_permissions(request, user, task, accepted_task_offer):
    if accepted_task_offer and accepted_task_offer.offerer == user.profile:
        for team in task.teams.all():
            for file in task.files.all():
                task_file_team_helper(request, team, file)
            team.write = request.POST.get('permission-upload-' + str(team.id)) or False
            team.save()


def task_file_team_helper(request, team, file):
    try:
        tft_string = 'permission-perobj-' + str(file.id) + '-' + str(team.id)
        tft_id = request.POST.get(tft_string)
        instance = TaskFileTeam.objects.get(id=tft_id)
    except Exception:
        instance = TaskFileTeam(
            file=file,
            team=team,
        )

    post_request = request.POST.get('permission-read-' + str(file.id) + '-' + str(team.id)) or False
    instance.read = post_request
    instance.write = post_request
    instance.modify = post_request
    instance.save()


def handle_team_add(request, user, accepted_task_offer):
    if accepted_task_offer and accepted_task_offer.offerer == user.profile:
        instance = get_object_or_404(Team, id=request.POST.get('team-id'))
        team_add_form = TeamAddForm(request.POST, instance=instance)
        if team_add_form.is_valid():
            team = team_add_form.save(False)
            team.members.add(*team_add_form.cleaned_data['members'])
            team.save()


def handle_team(request, user, task, accepted_task_offer):
    if accepted_task_offer and accepted_task_offer.offerer == user.profile:
        team_form = TeamForm(request.POST)
        if team_form.is_valid():
            team = team_form.save(False)
            team.task = task
            team.save()


def handle_delivery(request, user, task, accepted_task_offer):
    if accepted_task_offer and accepted_task_offer.offerer == user.profile:
        deliver_form = DeliveryForm(request.POST, request.FILES)
        if deliver_form.is_valid():
            delivery = deliver_form.save(commit=False)
            delivery.task = task
            delivery.delivery_user = user.profile
            delivery.save()
            task.status = "pa"
            task.save()


def handle_delivery_response(request, user, task):
    instance = get_object_or_404(Delivery, id=request.POST.get('delivery-id'))
    deliver_response_form = TaskDeliveryResponseForm(request.POST, instance=instance)
    if deliver_response_form.is_valid():
        delivery = deliver_response_form.save()
        delivery.responding_time = timezone.now()
        delivery.responding_user = user.profile
        delivery.save()

        if delivery.status == 'a':
            task.status = "pp"
            task.save()
        elif delivery.status == 'd':
            task.status = "dd"
            task.save()


def is_authorized_to_view(user_permissions):
    return user_permissions['read'] or \
           user_permissions['write'] or \
           user_permissions['modify'] or \
           user_permissions['owner'] or \
           user_permissions['view_task']


@login_required
def task_permissions(request, project_id, task_id):
    user = request.user
    task = Task.objects.get(pk=task_id)
    project = Project.objects.get(pk=project_id)
    accepted_task_offer = task.accepted_task_offer()
    if (project.user == request.user.profile or user == accepted_task_offer.offerer.user) and int(
            project_id) == task.project.id:
        if request.method == 'POST' and TaskPermissionForm(request.POST).is_valid():
            user_permissions_helper(task, TaskPermissionForm(request.POST))
            return redirect('task_view', project_id=project_id, task_id=task_id)

        task_permission_form = TaskPermissionForm()
        return render(
            request,
            'projects/task_permissions.html',
            {
                'project': project,
                'task': task,
                'form': task_permission_form,
            }
        )
    return redirect('task_view', project_id=project_id, task_id=task_id)


def user_permissions_helper(task, task_permission_form):
    username = task_permission_form.cleaned_data['user']
    user = User.objects.get(username=username)
    permission_type = task_permission_form.cleaned_data['permission']
    if permission_type == 'Read':
        task.read.add(user.profile)
    elif permission_type == 'Write':
        task.write.add(user.profile)
    elif permission_type == 'Modify':
        task.modify.add(user.profile)


@login_required
def delete_file(request, file_id):
    f = TaskFile.objects.get(pk=file_id)
    f.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
