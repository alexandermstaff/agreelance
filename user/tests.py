from unittest import skip
from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse
from projects.models import ProjectCategory, Rating, Project, Task


class SignUpTests(TestCase):

    def setUp(self):
        return

    def test_boundary(self):
        c = Client()
        category = ProjectCategory.objects.create(name='cool')
        c.post(reverse('signup'),
               {
                   'username': 'bobberson',
                   'first_name': 'følkajsdøflkjaølskdjføalksdjf',
                   'last_name': 'dudeman',
                   'password1': 'dfhfsgh456*efghdfgh567yetye',
                   'password2': 'dfhfsgh456*efghdfgh567yetye',
                   'email': 'a@a.com',
                   'email_confirmation': 'a@a.com',
                   'phone_number': 'hah',
                   'country': 'hmm',
                   'state': 'hmm',
                   'city': 'hmm',
                   'postal_code': 'hmm',
                   'street_address': 'hmm',
                   'categories': category.id,
               })
        self.assertEqual(User.objects.get(username='bobberson').username, 'bobberson')
        c.post(reverse('signup'),
               {
                   'username': 'bobberman',
                   'first_name': 'følkajsdøflkjaølskdjføalksdjfdd',
                   'last_name': 'dudeman',
                   'password1': 'dfhfsgh456efghdfgh567yetye',
                   'password2': 'dfhfsgh456efghdfgh567yetye',
                   'email': 'a@a.com',
                   'email_confirmation': 'a@a.com',
                   'phone_number': 'hah',
                   'country': 'hmm',
                   'state': 'hmm',
                   'city': 'hmm',
                   'postal_code': 'hmm',
                   'street_address': 'hmm',
                   'categories': category.id,
               })
        with self.assertRaisesMessage(User.DoesNotExist, 'User matching query does not exist.'):
            User.objects.get(username='bobberman')

    @skip('Email confirmation not validated')
    def test_domain_email(self):
        c = Client()
        category = ProjectCategory.objects.create(name='cool')
        c.post(reverse('signup'),
               {
                   'username': 'bobberson',
                   'first_name': 'følkajsdøflkjaølskdjføalksdjf',
                   'last_name': 'dudeman',
                   'password1': 'dfhfsgh456efghdfgh567yetye',
                   'password2': 'dfhfsgh456efghdfgh567yetye',
                   'email': 'this_email_address@a.com',
                   'email_confirmation': 'is_not_the_same@a.com',
                   'phone_number': 'hah',
                   'country': 'hmm',
                   'state': 'hmm',
                   'city': 'hmm',
                   'postal_code': 'hmm',
                   'street_address': 'hmm',
                   'categories': category.id,
               })
        with self.assertRaisesMessage(User.DoesNotExist, 'User matching query does not exist.'):
            User.objects.get(username='bobberson')

    def test_domain_password(self):
        c = Client()
        category = ProjectCategory.objects.create(name='cool')
        c.post(reverse('signup'),
               {
                   'username': 'bobberson',
                   'first_name': 'følkajsdøflkjaølskdjføalksdjf',
                   'last_name': 'dudeman',
                   'password1': 'dfhfsgh456*THESE_DONT*efghdfgh567yetye',
                   'password2': 'dfhfsgh456e*MATCH*fghdfgh567yetye',
                   'email': 'a@a.com',
                   'email_confirmation': 'a@a.com',
                   'phone_number': 'hah',
                   'country': 'hmm',
                   'state': 'hmm',
                   'city': 'hmm',
                   'postal_code': 'hmm',
                   'street_address': 'hmm',
                   'categories': category.id,
               })
        with self.assertRaisesMessage(User.DoesNotExist, 'User matching query does not exist.'):
            User.objects.get(username='bobberson')


class RatingTests(TestCase):

    def test_avg_rating(self):
        user_rated = User.objects.create(username='rated')
        user_rater = User.objects.create(username='rater')
        profile_rated = user_rated.profile
        profile_rater = user_rater.profile
        category = ProjectCategory.objects.create(name='test')
        project = Project.objects.create(title='owner_test', category=category, user=profile_rater)
        task1 = Task.objects.create(title='task1', project=project)
        task2 = Task.objects.create(title='task2', project=project)

        Rating.objects.create(user=profile_rated, task=task1, rater=profile_rater, rating=5)
        self.assertEqual(profile_rated.get_avg_rating(), 5)
        Rating.objects.create(user=profile_rated, task=task2, rater=profile_rater, rating=3)
        self.assertEqual(profile_rated.get_avg_rating(), 4)
