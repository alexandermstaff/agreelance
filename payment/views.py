from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from projects.forms import RatingForm
from projects.models import Project, Task, Rating
from projects.templatetags.project_extras import get_accepted_task_offer
from .forms import PaymentForm
from .models import Payment


@login_required
def payment(request, project_id, task_id):
    task = Task.objects.get(pk=task_id)
    sender = Project.objects.get(pk=project_id).user
    receiver = get_accepted_task_offer(task).offerer

    if request.method == 'POST':
        post_payment = Payment(payer=sender, receiver=receiver, task=task)
        post_payment.save()
        task.status = Task.PAYMENT_SENT
        task.save()

        return redirect('receipt', project_id=project_id, task_id=task_id)

    form = PaymentForm()

    return render(request,
                  'payment/payment.html', {
                      'form': form,
                  })


@login_required
def receipt(request, project_id, task_id):
    project = Project.objects.get(pk=project_id)
    task = Task.objects.get(pk=task_id)
    task_offer = get_accepted_task_offer(task)
    if request.method == 'POST':
        form = RatingForm(request.POST)
        if form.is_valid():
            Rating.objects.create(
                user=task_offer.offerer,
                task=task,
                rater=Project.objects.get(pk=project_id).user,
                rating=request.POST['rating']
            )
        return render(request,
                      'payment/receipt.html', {
                          'project': project,
                          'task': task,
                          'taskoffer': task_offer,
                          'form': form
                      })
    else:
        form = RatingForm()
        return render(request,
                      'payment/receipt.html', {
                          'project': project,
                          'task': task,
                          'taskoffer': task_offer,
                          'form': form,
                      })
