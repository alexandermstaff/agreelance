from django.shortcuts import render, redirect

from projects.models import Project, ProjectCategory


def home(request):
    if not request.user.is_authenticated:
        return redirect('projects')
    user = request.user
    user_projects = Project.objects.filter(user=user.profile)
    customer_projects = list(Project.objects.filter(participants__id=user.id).order_by().distinct())
    for team in user.profile.teams.all():
        customer_projects.append(team.task.project)
    cd = {}
    for customer_project in customer_projects:
        cd[customer_project.id] = customer_project

    customer_projects = cd.values()
    given_offers_projects = Project.objects.filter(pk__in=get_given_offer_projects(user)).distinct()
    if request.method == 'POST':
        ProjectCategory.objects.create(
            name=request.POST['new_category']
        )
        return render(
            request,
            'index.html',
            {
                'user_projects': user_projects,
                'customer_projects': customer_projects,
                'given_offers_projects': given_offers_projects,
            })
    return render(
        request,
        'index.html',
        {
            'user_projects': user_projects,
            'customer_projects': customer_projects,
            'given_offers_projects': given_offers_projects,
        })


def get_given_offer_projects(user):
    project_ids = set()

    for task_offer in user.profile.taskoffer_set.all():
        project_ids.add(task_offer.task.project.id)

    return project_ids
