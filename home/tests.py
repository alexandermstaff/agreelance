from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse

from projects.models import ProjectCategory


class CategoryTests(TestCase):

    def test_create(self):
        user = User.objects.create(username='admin')
        user.set_password('JR@eH@W6LA])7JB#?fzBJb96=?nxtMY.gEWn')
        user.save()
        c = Client()
        c.login(username='admin', password='JR@eH@W6LA])7JB#?fzBJb96=?nxtMY.gEWn')
        c.post(reverse('home'), {'new_category': 'beans'})
        self.assertEqual(ProjectCategory.objects.get(name='beans').name, 'beans')
